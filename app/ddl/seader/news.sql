-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 23 2017 г., 22:17
-- Версия сервера: 5.7.19-0ubuntu0.16.04.1
-- Версия PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `news`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tblNews`
--

CREATE TABLE `tblNews` (
  `id` int(11) NOT NULL,
  `imageUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `createdNews` datetime NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `tblNews`
--

INSERT INTO `tblNews` (`id`, `imageUrl`, `title`, `createdNews`, `description`, `active`) VALUES
(1, 'dtPeDe9tBlw.jpg', 'Первая новость', '2017-08-20 22:44:28', 'Hello World!', 1),
(2, NULL, 'Вторая новость', '2017-08-21 12:34:19', 'Hello World!', 1),
(3, '2.jpg', 'Новость от пользователя', '2017-08-23 11:09:20', 'А эта новость ещё не подтверждена', 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tblNews`
--
ALTER TABLE `tblNews`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_10F6292B36786B` (`title`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tblNews`
--
ALTER TABLE `tblNews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
