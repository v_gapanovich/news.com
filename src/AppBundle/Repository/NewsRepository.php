<?php

namespace AppBundle\Repository;

use AppBundle\Entity\News;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


class NewsRepository extends EntityRepository
{
    public function getAllNews(): array
    {
        $qb = $this->createQueryBuilder('n');
        $qe = $qb->expr();

        return $qb
            ->orderBy('n.createdNews', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getAllActiveNews(): array
    {
        $qb = $this->createQueryBuilder('n');
        $qe = $qb->expr();

        return $qb
            ->where($qe->eq('n.active', ':active'))
            ->setParameter(':active', true)
            ->orderBy('n.createdNews', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
