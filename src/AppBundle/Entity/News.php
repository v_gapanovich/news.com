<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Interfaces\UploadFileInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Traits\UploadFileTrait;
use Doctrine\ORM\Mapping as ORM;
use \DateTime;

/**
 * Admin
 *
 * @ORM\Table(name="tblNews")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsRepository")
 */
class News implements UploadFileInterface
{
    use UploadFileTrait;
    const UPLOADED_ROOT_DIR = 'uploads/ImageForNews';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="imageUrl", type="string", length=255, nullable=true)
     */
    private $imageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=140, unique=true)
     *
     * @Assert\NotNull(message="The name not can empty")
     * @Assert\Length(
     *      min=4,
     *      max=140,
     *      minMessage="Title is too short",
     *      maxMessage="The title is too long")
     */
    private $title;

    /**
     * @Gedmo\Timestampable(on="create")
     *
     * @var \DateTime
     *
     * @ORM\Column(name="createdNews", type="datetime")
     */
    private $createdNews;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     *
     * @Assert\Type(type="string")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="integer", length=1)
     *
     * @Assert\Type(type="integer")
     */
    private $active;

    /**
     * News constructor.
     */
    public function __construct() {
        $this->setCreatedNews(new \DateTime());
        $this->setActive(0);
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedNews(): \DateTime
    {
        return $this->createdNews;
    }

    /**
     * @param DateTime $date
     *
     * @return self
     */
    public function setCreatedNews(\DateTime $date): self
    {
        $this->createdNews = $date;

        return $this;
    }

    /**
     * return void
     */
    public function generateHash(): void
    {
        $this->hash = uniqid();
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return integer
     */
    public function getActive(): int
    {
        return $this->active;
    }

    /**
     * @param integer $active
     */
        public function setActive(int $active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl(string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return self::UPLOADED_ROOT_DIR . '/' . $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        return $this->imageUrl;
    }

    /**
     * @return null|string
     */
    public function getUploadRootDir(): ?string
    {
        return self::UPLOADED_ROOT_DIR;
    }

    /**
     * @param string $path
     */
    public function setFilePath(string $path): void
    {
        $this->imageUrl = $path;
    }
}
