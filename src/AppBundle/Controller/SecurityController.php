<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Exception;

/**
 * Class SecurityController.
 */
class SecurityController extends Controller
{
    /**
     * @throws Exception
     */
    public function loginCheckAction()
    {
        throw new Exception('This should never be reached!');
    }
    
    /**
     * @Template("login.html.twig")
     */
    public function loginAction()
    {
        return [];
    }

    /**
     * @throws Exception
     */
    public function logoutAction()
    {
        throw new Exception('This should never be reached!');
    }
}
