<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\CreateAndEditNewsType;
use AppBundle\Entity\News;

class CalculatorController extends Controller
{

    /**
     * @Template("calculator.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function calculatorAction(Request $request): array
    {
        $errors = [];
        $news = new News();

        $em = $this->getDoctrine()->getManager();

        $newsForEdit = $em->getRepository(News::class)->getAllNews();

        $form = $this->createForm(CreateAndEditNewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($news);

            if (!$errors->count()) {
                $news->upload();
                $em->persist($news);
                $em->flush();
            } else {
                foreach ($form->getErrors() as $error) {
                    $errors[] = $error->getMessage();
                }
            }
        }

        return [
            'addNewsForm' => $form->createView(),
            'errors' => $errors,
            'news' => $news,
            'newsForEdit' => $newsForEdit
        ];
    }
}
