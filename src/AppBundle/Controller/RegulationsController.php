<?php

namespace AppBundle\Controller;

use AppBundle\Entity\News;
use AppBundle\Form\CreateAndEditNewsType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RegulationsController extends Controller
{
    /**
     * @Template("regulations.html.twig")
     *
     * @return array
     */
    public function regulationsAction()
    {
        $errors = [];
        $news = new News();
        $form = $this->createForm(CreateAndEditNewsType::class, $news);

        return ['addNewsForm' => $form->createView(), 'errors' => $errors];
    }
}
