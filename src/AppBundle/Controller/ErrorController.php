<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ErrorController extends Controller
{
    public function errorAction()
    {
      return $this->render('404.html.twig');
    }
}
