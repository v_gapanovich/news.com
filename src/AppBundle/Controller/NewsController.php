<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\CreateAndEditNewsType;
use AppBundle\Entity\News;

class NewsController extends Controller
{
    /**
     * @Template("createNews.html.twig")
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function createNewsByAdminAction(Request $request)
    {
        $errors = [];
        $news = new News();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CreateAndEditNewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($news);

            if (!$errors->count()) {
                $news->upload();
                $news->setActive(1);
                $em->persist($news);
                $em->flush();

                return $this->redirectToRoute('homepage');
            }
        } else {
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return [
            'addNewsForm' => $form->createView(),
            'errors' => $errors,
            'news' => $news
        ];
    }

    /**
     * @Template("main.html.twig")
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function createNewsByAnonymousAction(Request $request)
    {
        $errors = [];
        $news = new News();
        $em = $this->getDoctrine()->getManager();
        $newsForEdit = $em->getRepository(News::class)->getAllActiveNews();
        $form = $this->createForm(CreateAndEditNewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($news);

            if (!$errors->count()) {
                $news->upload();
                $em->persist($news);
                $em->flush();

                return $this->redirectToRoute('homepage');
            } else {
                foreach ($form->getErrors() as $error) {
                    $errors[] = $error->getMessage();
                }
            }
        }

        return [
            'addNewsForm' => $form->createView(),
            'errors' => $errors,
            'news' => $news,
            'newsForEdit' => $newsForEdit
        ];
    }

    /**
     * @Template("editNews.html.twig")
     *
     * @param Request $request
     * @param News    $news
     *
     * @return array|RedirectResponse
     */
    public function editNewsAction(Request $request, News $news)
    {
        $errors = [];
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CreateAndEditNewsType::class, $news);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($news);

            if (!$errors->count()) {
                $news->upload();
                $em->flush();

                return $this->redirectToRoute('homepage');
            } else {
                foreach ($form->getErrors() as $error) {
                    $errors[] = $error->getMessage();
                }
            }
        }

        return [
            'newsForEdit' => $news,
            'newsForm' => $form->createView(),
            'errors' => $errors
        ];
    }

    /**
     * @Template("adminNewsForChange.html.twig")
     *
     * @return array
     */
    public function allNewsForChangeAction(): array
    {
        return ['newsForEdit' => $this->getDoctrine()->getManager()->getRepository(News::class)->getAllNews()];
    }

    /**
     * @param News $news
     *
     * @return RedirectResponse
     */
    public function newsConfirmationAction(News $news): RedirectResponse
    {
        $news->setActive(1);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('show_admin_news');
    }

    /**
     * @param News $news
     *
     * @return RedirectResponse
     */
    public function deleteNewsAction(News $news): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($news);
        $em->flush();

        return $this->redirectToRoute('show_admin_news');
    }
}