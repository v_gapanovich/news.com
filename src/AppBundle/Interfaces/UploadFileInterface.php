<?php

namespace AppBundle\Interfaces;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Interface UploadFileInterface.
 */
interface UploadFileInterface
{
    /**
     * @return string
     */
    public function getFilePath(): ?string;

    /**
     * @return File|null
     */
    public function getFile(): ?File;

    /**
     * @return null|string
     */
    public function getUploadRootDir(): ?string;

    /**
     * @param string $path
     */
    public function setFilePath(string $path): void;

    /**
     * @return void
     */
    public function resetFile(): void;
}