var app = angular.module('MenuStyleApp', []);

app.controller('ActiveMenuController', function($scope) {
  $scope.active = '';

  $scope.activeMenu = function () {
	  $scope.active = 'active';
  }

  $scope.passiveMenu = function () {
	  $scope.active = '';
  }

});

angular.module('LoginFormStyleApp', [])
    .controller('ValidateController', function($scope) {
      $scope.validateUsername = '';

      $scope.usernameValid = function (length) {
        if(length > 3) {
          $scope.validateUsername = 'text-success-800';
          return '';
        }
        if(length < 4) {
          $scope.validateUsername = 'text-danger-800';
          return '';
        }
        return $scope.validateUsername = '';
      }

      $scope.passwordValid = function (length) {
        if(length > 5) {
          $scope.validatePassword = 'text-success-800';
          return '';
        }
        if(length < 6) {
          $scope.validatePassword = 'text-danger-800';
          return '';
        }
        return $scope.validatePassword = '';
      }

    });


